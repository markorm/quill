﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Quill.Server.DataAccess;
using Quill.Shared.Models;
using System.Threading.Tasks;

namespace Quill.Server.WebApi.Controllers
{

    [Route("api/v1/security")]
    public class SecurityController: QuillApiBase
    {

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="authManager"></param>
        /// <param name="userApi"></param>
        public SecurityController(
            QuillConfig config,
            UserApi userApi)
            : base(config, userApi)
        {
            _userApi = userApi;
        }

        /// <summary>
        /// Data access for User entities.
        /// </summary>
        private UserApi _userApi;

        /// <summary>
        /// Process user login.
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        [HttpPost, Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest  loginRequest)
        {
            LoginResult loginResult = _userApi.Login(loginRequest);
            if (loginResult != null) { return Ok(loginResult); }
            return Forbid();
        }

        /// <summary>
        /// Register a user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost, Route("register")]
        public async Task<IActionResult> Register([FromBody] User user)
        {
            User createdUser = _userApi.CreateUser(user);
            if (createdUser != null) return Created($"/api/v1/users/{createdUser.ID}", createdUser);
            return BadRequest();
        }

    }

}
