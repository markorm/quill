﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Quill.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quill.Server.WebApi.Controllers
{

    [ApiController]
    [Route("api/v1/projects")]
    public class ProjectsController: ControllerBase
    {

        [HttpGet, Authorize]
        public async Task<IActionResult> GetProjects()
        {
            var projects = new List<Project> { new Project(), new Project() };
            return Ok(projects);
        }

    }

}
