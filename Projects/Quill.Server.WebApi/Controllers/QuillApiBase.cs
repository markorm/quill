﻿using Microsoft.AspNetCore.Mvc;
using Quill.Server.DataAccess;
using Quill.Shared.Models;

namespace Quill.Server.WebApi.Controllers
{

    /// <summary>
    /// The base api controller for the Quill Apis.
    /// </summary>
    public abstract class QuillApiBase: ControllerBase
    {

        public QuillApiBase(
            QuillConfig config,
            UserApi userApi)
        {
            _config = config;
            _userApi = userApi;
        }

        private readonly UserApi _userApi;

        private readonly QuillConfig _config;

    }

}
