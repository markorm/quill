﻿CREATE PROCEDURE [dbo].[UserCreate]
	@email VARCHAR(100) ,
	@password VARCHAR(4086),
	@firstName VARCHAR(100),
	@lastName VARCHAR(100),
	@middleName VARCHAR(100) NULL,
	@phone VARCHAR(30) NULL
AS
BEGIN

	-- Ensure the user doesn't already exist.
	IF EXISTS (SELECT 1 FROM [dbo].[User] where [Email] = @email) BEGIN
		SELECT -1;
		RETURN -1;
	END

	-- Create the user.
	INSERT INTO [dbo].[User] (
		[Email],
		[FirstName],
		[LastName],
		[MiddleName],
		[Phone],
		[Password],
		[CreatedDate]
	)
	VALUES (
		@email,
		@firstName,
		@lastName,
		@middleName,
		@phone,
		@password,
		GETDATE()
	);

	-- Return identity
	SELECT SCOPE_IDENTITY();

END
GO
