﻿using Quill.Shared.Models;

namespace Quill.Server.DataAccess
{

    /// <summary>
    /// The base call for data access classes.
    /// </summary>
    public abstract class ApiBase
    {

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="authManager"></param>
        public ApiBase(QuillConfig config)
        {
            _config = config;
        }

        protected readonly QuillConfig _config;

    }

}
