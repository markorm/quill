﻿using Dapper;
using Microsoft.IdentityModel.Tokens;
using Quill.Shared.Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Quill.Server.DataAccess
{

    /// <summary>
    /// The users data access layer.
    /// </summary>
    public class UserApi : ApiBase
    {

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="quillConfig"></param>
        /// <param name="authManager"></param>
        public UserApi(QuillConfig quillConfig) : base(quillConfig) { }

        /// <summary>
        /// Retrieve a single user by id.
        /// </summary>
        /// <returns></returns>
        public User RetrieveUserByID(int userID)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.Open();
                return conn.QuerySingle<User>("[dbo].[UserRetrieve]", new { userID }, commandType: CommandType.StoredProcedure);
            }
        }

        /// <summary>
        /// Create a new user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public User CreateUser(User user)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.Open();
                int userID = conn.QuerySingle<int>("[dbo].[UserCreate]", user, commandType: CommandType.StoredProcedure);
                return RetrieveUserByID(userID);
            }
        }

        /// <summary>
        /// Get a JWT token for a user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GetJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_config.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
                    new Claim(ClaimTypes.Name, user.Email)
                }),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        /// <summary>
        /// Processes user login.
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        public LoginResult Login(LoginRequest loginRequest)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.Open();
                var args = new { loginRequest.Email, Password = Scramble(loginRequest.Password) };
                int? userID = conn.QuerySingle<int?>("[dbo].[UserAuthenticate]", args, commandType: CommandType.StoredProcedure);
                if (userID == null) { return null; }
                User user = RetrieveUserByID((int)userID);
                return new LoginResult
                {
                    User = user,
                    Token = GetJwtToken(user)
                };
            }
        }

        /// <summary>
        /// Scramble a string.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns></returns>
        private string Scramble(string input)
        {
            return input;
        }

        /// <summary>
        /// Unscramble a string.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private string Unscramble(string input)
        {
            return input;
        }

    }

}
