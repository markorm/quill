﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quill.Shared.Models
{


    public class LoginResult
    {

        public User User { get; set; }

        public string Token { get; set; }

    }

}
