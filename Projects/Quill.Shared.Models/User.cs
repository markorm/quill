﻿using System;

namespace Quill.Shared.Models
{

    /// <summary>
    /// Class used for general access/identity.
    /// </summary>
    public class User
    {

        /// <summary>
        /// The unique userID.
        /// </summary>
        public int ID { get; set; }
        
        /// <summary>
        /// The user's email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The user's first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The user's last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// The user's phone numnber.
        /// </summary>
        public string Phone { get; set; }

    }

}
