﻿using Blazor.Fluxor;
using Quill.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quill.Client.Web.State
{

    /// <summary>
    /// The main client application class.
    /// </summary>
    public class QuillState
    {

        public User User { get; set; }

        public bool IsAuthenticated { get; private set; }

    }

    /// <summary>
    /// The main state management class.
    /// </summary>
    public class Quill: Feature<QuillState>
    {
        public override string GetName() => "Quill";
        protected override QuillState GetInitialState() => new QuillState();
    }



}
